ls//let name='Mosh'; // string
//let age= 30; //Number
//let isApproved= true; //Boolean can be (true/false)
//let fristname= undefined;
//let lastname=null;
//let name= 'Mosh';
//let age= 30;
let person={
    name: "Mosh",
    age: 30
};
//Dot notation
person.name ='John';
//Bracket Notation
let selection= 'name';
person[selection]='Mary';
console.log(person.name);
let selectedcolors=['red','blue'];
selectedcolors[2]='green';
console.log(selectedcolors.length);
function greet(name, lastname)
{
    console.log('Hello ' + name+' '+lastname);
}
greet('Ujwal', 'Saurav');
greet('Mary','Smith');
function square(number)
{
    return number*number;
}
console.log(square(2));